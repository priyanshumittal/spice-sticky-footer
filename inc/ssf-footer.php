<?php
add_action('spice_sticky_footer','spice_sticky_footer_callback');	
function spice_sticky_footer_callback()
{
	if(get_theme_mod('sticky_footer_enable',false) == true ):?>
		<style type="text/css">
			#spice-footer-bar { opacity: <?php echo esc_attr(get_theme_mod('ssf_opacity',0.9));?>;}
		</style>
		<div id="spice-footer-bar" class="<?php if(get_theme_mod('sticky_footer_text_mbl_enable',false) == false ) { echo "hide-from-spice-footer";} ?>">
			<ul class="spice-footer-icom <?php echo get_theme_mod('sticky_footer_enable',false);?>">
				<li class="spice-btn"><a href="#"><i class="<?php echo get_theme_mod('ssf_icon_class','fas fa-angle-double-up');?>"></i></a></li>
			</ul>
			<ul class="spice-text">
				<li><?php echo get_theme_mod('ssf_device_type','Copyright 2022 - Spicethemes Theme');?></li>
			</ul>
		</div>
		
		<?php $ssf_enable_sticky_footer_clr       =   get_theme_mod('enable_sticky_footer_clr', false); 
		if($ssf_enable_sticky_footer_clr == true) { ?>
			<style type="text/css">
				#spice-footer-bar 
				{
					background-color: <?php echo esc_attr(get_theme_mod('ssf_bg_color','#3d3d3d'));?>;
				}
				#spice-footer-bar .spice-text li, #spice-footer-bar .spice-text p
				{
					color: <?php echo esc_attr(get_theme_mod('ssf_text_color','#fff'));?>;
				}
				#spice-footer-bar .spice-text a
				{
					color: <?php echo esc_attr(get_theme_mod('ssf_link_color','#fff'));?>;
				}
				#spice-footer-bar .spice-text a:hover
				{
					color: <?php echo esc_attr(get_theme_mod('ssf_link_hover_color','#ff6f61'));?>;
				}
				#spice-footer-bar .spice-btn a
				{
				   	background-color: <?php echo esc_attr(get_theme_mod('ssf_btn_bg_color','#ff6f61'));?>;
				    color: <?php echo esc_attr(get_theme_mod('ssf_btn_color','#f9a69d'));?>;
				}
	    	</style>
	    <?php } 
	    $ssf_enable_sticky_footer_typo       =   get_theme_mod('enable_sticky_footer_typo', false); 
		if($ssf_enable_sticky_footer_typo == true) {
	    ?>
	    <style type='text/css' id="ssf-typo">
		    #spice-footer-bar ul.spice-text li, #spice-footer-bar ul.spice-text p
			{
				font-family: '<?php echo esc_attr(get_theme_mod('ssf_fontfamily','Poppins'));?>';
				font-size: <?php echo esc_attr(get_theme_mod('ssf_fontsize',16));?>px;
				font-weight: <?php echo esc_attr(get_theme_mod('ssf_fontweight',400));?>;
				font-style: <?php echo esc_attr(get_theme_mod('ssf_fontstyle','normal'));?>;
				text-transform: <?php echo esc_attr(get_theme_mod('ssf_transform','default'));?>;
			}
	        <?php
	        echo "@import url('https://fonts.googleapis.com/css2?family=".esc_attr(get_theme_mod('ssf_fontfamily','Poppins')).":wght@100;200;300;400;500;600;700;800;900&display=swap');"; ?>
    	</style>
	<?php } endif;
}