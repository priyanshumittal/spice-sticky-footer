<?php
// Adding customizer settings
function ssf_customizer_controls($wp_customize)
{
    $ssf_font_size = array();
    for($i=10; $i<=100; $i++) {
      $ssf_font_size[$i] = $i;
    }
    $ssf_line_height = array();
    for($i=10; $i<=100; $i++) {
        $ssf_line_height[$i] = $i;
    }
    $ssf_font_style = array('normal'=>'Normal','italic'=>'Italic');
    $ssf_text_transform = array('default'=>'Default','capitalize'=>'Capitalize','lowercase'=>'Lowercase','Uppercase'=>'Uppercase');
    $ssf_font_weight = array('100'=>'100','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700','800'=>'800','900'=>'900');
    $ssf_font_family = ssf_typo_fonts();
 
    /***** STICKY FOOTER PANEL *****/
    $wp_customize->add_panel('ssf_panel',
        array(
            'priority' => 140,
            'capability' => 'edit_theme_options',
            'title' => esc_html__('Sticky Footer','spice-sticky-footer')
        ));


    /* ===================================================================================================================
    * Start Sticky General Setting *
    ====================================================================================================================== */
    $wp_customize->add_section( 'ssf_customizer_section' , array(
        'title'      => esc_html__('General Settings', 'spice-sticky-footer'),
        'priority'   => 1,
        'panel'  => 'ssf_panel',
    ) );


    /* Enable Sticky Footer */
    $wp_customize->add_setting( 'sticky_footer_enable', 
    array(
        'default'           => false,
        'sanitize_callback' => 'ssf_sanitize_checkbox'
    ));
    $wp_customize->add_control(new SSF_Toggle_Control( $wp_customize, 'sticky_footer_enable',
        array(
            'label'     =>  esc_html__( 'Enable/Disable Sticky Footer', 'spice-sticky-footer'  ),
            'section'   =>  'ssf_customizer_section',
            'setting'   =>  'sticky_footer_enable',
            'type'      =>  'toggle',
            'priority'          =>  1
        )
    ));


    /* Enable text on mobile Sticky Footer */
    $wp_customize->add_setting( 'sticky_footer_text_mbl_enable', 
    array(
        'default'           => false,
        'sanitize_callback' => 'ssf_sanitize_checkbox'
    ));
    $wp_customize->add_control(new SSF_Toggle_Control( $wp_customize, 'sticky_footer_text_mbl_enable',
        array(
            'label'     =>  esc_html__( 'Enable/Disable text on mobile', 'spice-sticky-footer'  ),
            'section'   =>  'ssf_customizer_section',
            'active_callback' =>  'ssf_plugin_callback',
            'setting'   =>  'sticky_footer_text_mbl_enable',
            'type'      =>  'toggle',
            'priority'  =>  2
        )
    ));

   /* Enter your icon class */
    $wp_customize->add_setting('ssf_icon_class',
        array(
            'default'           =>  'fas fa-angle-double-up',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'sanitize_text_field'
        )
    );
    $wp_customize->add_control('ssf_icon_class', 
        array(
            'label'             =>  esc_html__('Enter your Font Awesome icon class','spice-sticky-footer' ),
            'active_callback'   => 'ssf_plugin_callback',
            'section'           => 'ssf_customizer_section',
            'setting'           => 'ssf_icon_class',
            'type'              => 'text',
            'priority'          =>  5,
        )
    );


    /* Sticky Footer opacity */
    $wp_customize->add_setting('ssf_opacity',
        array(
            'default'       =>  0.9,
            'capability'    =>  'edit_theme_options'
        )
    );
    $wp_customize->add_control( new SSF_Slider_Custom_Control( $wp_customize, 'ssf_opacity',
        array(
            'label'             =>  esc_html__( 'Footer Opacity', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_plugin_callback',
            'section'           =>  'ssf_customizer_section',
            'setting'           =>  'ssf_opacity',
            'priority'          =>  6,
            'input_attrs'   => array(
                'min'   => 0.1,
                'max'   => 1.0,
                'step'  => 0.1
            ),
        )
    ));


    // Enable sticky footer on Desktop & Mobile view
    $wp_customize->add_setting('ssf_device_type',
        array(
            'default'           =>  __( 'Copyright 2022 - Spicethemes Theme', 'spice-sticky-footer' ),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'ssf_sanitize_text'
        )
    );
    $wp_customize->add_control('ssf_device_type', 
        array(
            'label'             =>  esc_html__('Enter your custom text','spice-sticky-footer' ),
            'active_callback'   =>  'ssf_plugin_callback',
            'section'           =>  'ssf_customizer_section',
            'setting'           =>  'ssf_device_type',
            'type'              =>  'textarea',
            'priority'          =>  9,
        )
    );
    /* ===================================================================================================================
    * End Sticky General Setting *
    ====================================================================================================================== */



    /* ===================================================================================================================
    * Start Sticky Footer Typo*
    ====================================================================================================================== */

    $wp_customize->add_section('ssf_typo_section', 
        array(
            'title'     => esc_html__('Typography Settings', 'spice-sticky-footer' ),
            'panel'  => 'ssf_panel',
            'priority'  => 2
        )
    );
    $wp_customize->add_setting('enable_sticky_footer_typo',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'ssf_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new SSF_Toggle_Control( $wp_customize, 'enable_sticky_footer_typo',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-sticky-footer'  ),
            'section'   =>  'ssf_typo_section',
            'setting'   =>  'enable_sticky_footer_typo',
            'type'      =>  'toggle'
        )
    ));

    $wp_customize->add_setting(
    'ssf_fontfamily',
    array(
        'default'           =>  'Poppins',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'sanitize_text_field',
        )   
    );
    $wp_customize->add_control('ssf_fontfamily', array(
            'label' => esc_html__('Font family','spice-sticky-footer'),
            'section' => 'ssf_typo_section',
            'setting' => 'ssf_fontfamily',
            'type'    =>  'select',
            'choices'=>$ssf_font_family,
            'active_callback'   =>  'ssf_typo_callback',
    ));

    $wp_customize->add_setting(
    'ssf_fontsize',
    array(
        'default'           =>  16,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'ssf_sanitize_select',
        )   
    );

    $wp_customize->add_control('ssf_fontsize', array(
            'label' => __('Font size (px)','spice-sticky-footer'),
            'section' => 'ssf_typo_section',
            'setting' => 'ssf_fontsize',
            'type'    =>  'select',
            'choices'=>$ssf_font_size,
            'active_callback'   =>  'ssf_typo_callback',
        ));
    $wp_customize->add_setting(
        'ssf_fontweight',
        array(
            'default'           =>  400,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'ssf_sanitize_select',
        )   
    );
    $wp_customize->add_control('ssf_fontweight', array(
            'label' => __('Font Weight','spice-sticky-footer'),
            'section' => 'ssf_typo_section',
            'setting' => 'ssf_fontweight',
            'type'    =>  'select',
            'choices'=>$ssf_font_weight,
            'active_callback'   =>  'ssf_typo_callback',
    ));
    $wp_customize->add_setting(
        'ssf_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'sanitize_text_field',
        )   
    );
    $wp_customize->add_control('ssf_fontstyle', array(
            'label' => __('Font style','spice-sticky-footer'),
            'section' => 'ssf_typo_section',
            'setting' => 'ssf_fontstyle',
            'type'    =>  'select',
            'choices'=>$ssf_font_style,
            'active_callback'   =>  'ssf_typo_callback',
    ));
    $wp_customize->add_setting(
        'ssf_transform',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'sanitize_text_field',
        )   
    );
    $wp_customize->add_control('ssf_transform', array(
            'label' => __('Text Transform','spice-sticky-footer'),
            'section' => 'ssf_typo_section',
            'setting' => 'ssf_transform',
            'type'    =>  'select',
            'choices'=>$ssf_text_transform,
            'active_callback'   =>  'ssf_typo_callback',
    ));

    /* ===================================================================================================================
    * End Sticky Footer Typo *
    ====================================================================================================================== */




    /* ====================
    * Sticky Color Setting 
    ==================== */
    $wp_customize->add_section('ssf_clr_section', 
        array(
            'title'     => esc_html__('Color Settings', 'spice-sticky-footer' ),
            'panel'  => 'ssf_panel',
            'priority'  => 2
        )
    );
    
    $wp_customize->add_setting('enable_sticky_footer_clr',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'ssf_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new SSF_Toggle_Control( $wp_customize, 'enable_sticky_footer_clr',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'spice-sticky-footer'  ),
            'section'   =>  'ssf_clr_section',
            'setting'   =>  'enable_sticky_footer_clr',
            'type'      =>  'toggle'
        )
    ));
    
    class SSFMenu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Menu', 'spice-sticky-footer' ); ?></h3>
        <?php }
    }

     $wp_customize->add_setting('ssf_bg_color', 
        array(
            'default'           => '#3d3d3d',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new SSF_Alpha_Color_Control($wp_customize, 'ssf_bg_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_bg_color'
        )
    ));

    $wp_customize->add_setting('ssf_text_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ssf_text_color', 
        array(
            'label'             =>  esc_html__('Text Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_text_color'
        )
    ));

    $wp_customize->add_setting('ssf_link_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ssf_link_color', 
        array(
            'label'             =>  esc_html__('Link Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_link_color'
        )
    ));

    $wp_customize->add_setting('ssf_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ssf_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_link_hover_color'
        )
    ));


    $wp_customize->add_setting('ssf_btn_bg_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ssf_btn_bg_color', 
        array(
            'label'             =>  esc_html__('Button Background Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_btn_bg_color'
        )
    ));

    $wp_customize->add_setting('ssf_btn_color', 
        array(
            'default'           => '#f9a69d',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ssf_btn_color', 
        array(
            'label'             =>  esc_html__('Button Color', 'spice-sticky-footer' ),
            'active_callback'   =>  'ssf_color_callback',
            'section'           =>  'ssf_clr_section',
            'setting'           =>  'ssf_btn_color'
        )
    ));




}

add_action( 'customize_register', 'ssf_customizer_controls' );