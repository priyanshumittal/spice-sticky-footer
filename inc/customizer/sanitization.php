<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


// callback function for the sticky footer
function ssf_plugin_callback($control) {
    if (false == $control->manager->get_setting('sticky_footer_enable')->value()) {
        return false;
    } else {
        return true;
    }
}


// callback function for the footer typography
function ssf_typo_callback($control) {
    if (false == $control->manager->get_setting('enable_sticky_footer_typo')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the footer background color
function ssf_color_callback($control) {
    if (false == $control->manager->get_setting('enable_sticky_footer_clr')->value()) {
        return false;
    } else {
        return true;
    }
}


/**
 * Text sanitization callback
*/
function ssf_sanitize_text($input) {

    return wp_kses_post(force_balance_tags($input));

}

/**
 * Select choices sanitization callback
*/
function ssf_sanitize_select($input, $setting) {

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control($setting->id)->choices;

    //return input if valid or return default option
    return ( array_key_exists($input, $choices) ? $input : $setting->default );
}

/**
 * Checkbox sanitization callback
*/
function ssf_sanitize_checkbox($checked) {

    // Boolean check.
    return ( ( isset($checked) && true == $checked ) ? true : false );

}