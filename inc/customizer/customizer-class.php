<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Spice_Sticky_Footer_Customizer' ) ) :

	/**
	 * Spice Sticky Footer Customizer class
	*/
	class Spice_Sticky_Footer_Customizer {

		/**
		 * Setup class
		*/
		public function __construct() 
		{
			add_action( 'customize_register', array( $this, 'ssf_custom_controls' ) );
			add_action( 'after_setup_theme', array( $this, 'ssf_register_options' ) );
			add_action( 'wp_enqueue_scripts', array( $this,'ssf_load_script'));
			add_action( 'admin_enqueue_scripts', array( $this,'ssf_load_admin_script'));
		}


		/**
		 * Adds custom controls
		*/
		public function ssf_custom_controls( $wp_customize ) 
		{
			require_once ( SSF_PLUGIN_DIR . '/inc/customizer/controls/toggle/class-toggle-control.php' );
			require_once ( SSF_PLUGIN_DIR . '/inc/customizer/controls/range/range-control.php' );
			require_once ( SSF_PLUGIN_DIR . '/inc/customizer/controls/color/color-control.php' );
			$wp_customize->register_control_type('SSF_Toggle_Control');
		}

		/**
		 * Adds customizer options
		*/
		public function ssf_register_options() 
		{
			require_once ( SSF_PLUGIN_DIR . '/inc/customizer/sanitization.php' );
			require_once ( SSF_PLUGIN_DIR . '/inc/customizer/customizer.php' );
		}

		/**
		 * Load Js
		*/
		public function ssf_load_script()
	     {
	     if(get_theme_mod('sticky_footer_enable',false) == true ):	
		  	  wp_enqueue_script('ssf-custom', SSF_PLUGIN_URL . 'js/custom.js', array('jquery'));
		      wp_enqueue_style('ssf-custom',  SSF_PLUGIN_URL. 'css/custom.css');
	  	 endif;
	     }

	     public function ssf_load_admin_script()
	     {
	     	wp_enqueue_style('ssf-admin',  SSF_PLUGIN_URL. 'css/admin.css');

	     }

	}

endif;

new Spice_Sticky_Footer_Customizer();	