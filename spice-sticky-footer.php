<?php 
/*
* Plugin Name:				Spice Sticky Footer
* Plugin URI:  			
* Description: 				Enhances functionality of footer.
* Version:     				1.0
* Requires at least:        5.3
* Requires PHP: 			5.2
* Tested up to: 			6.2
* Author:      				Spicethemes
* Author URI:  				https://spicethemes.com
* License: 				  	GPLv2 or later
* License URI: 				https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 				spice-sticky-footer
* Domain Path:  			/languages
*/

// Freemius SDK Snippet
if ( ! function_exists( 'ssf_fs' ) ) {
    // Create a helper function for easy SDK access.
    function ssf_fs() {
        global $ssf_fs;

        if ( ! isset( $ssf_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $ssf_fs = fs_dynamic_init( array(
                'id'                  => '10575',
                'slug'                => 'spice-sticky-footer',
                'premium_slug'        => 'spice-sticky-footer',
                'type'                => 'plugin',
                'public_key'          => 'pk_685ef2986180072080d00dc761a43',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }

        return $ssf_fs;
    }
  ssf_fs();
}

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// define the constant for the URL
define( 'SSF_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'SSF_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

add_action( 'init', 'ssf_load_textdomain' );
/**
 * Load plugin textdomain.
 */
function ssf_load_textdomain() {
  load_plugin_textdomain( 'ssf', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

//Define the function for the plugin activation
function ssf_activation() {

	require_once SSF_PLUGIN_DIR . 'inc/customizer/customizer-class.php';

	//Font file
	require_once SSF_PLUGIN_DIR . 'inc/ssf-fonts.php';

	//Sticky Footer
	require_once SSF_PLUGIN_DIR . 'inc/ssf-footer.php';

}
add_action('plugins_loaded', 'ssf_activation');