=== Spice Sticky Footer ===

Contributors: 		spicethemes
Tags: 				sticky footer
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.2
Stable tag: 		1.0
License: 			GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Enhances functionality of footer.

== Changelog ==

@Version 1.0
* Updated freemius directory.

@Version 0.2
* Updated Freemius code.

@Version 0.1
* Initial Release.

======= External Resources =======

Font Awesome:
Copyright: (c) Dave Gandy
License: https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
Source: https://fontawesome.com